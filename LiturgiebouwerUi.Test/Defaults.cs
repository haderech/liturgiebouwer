using LiturgiebouwerUi.Models;

namespace LiturgiebouwerUi.Test
{
    internal static class Defaults
    {
        public static readonly Book Book = new(10, "Henkies");
        public static readonly Chapter Chapter = new(Book, 5);
    }
}
