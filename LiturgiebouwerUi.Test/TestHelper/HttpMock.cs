using System;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using NSubstitute;
using Xunit;

namespace LiturgiebouwerUi.Test.TestHelper
{
    public class HttpMock
    {
        private static readonly Uri DefaultBaseAddress = new("https://127.0.0.1");
        private readonly HttpMessageHandler _handlerMock = Substitute.For<HttpMessageHandler>();

        public HttpClient Client() => Client(handler => new HttpClient(handler));

        public T Client<T>(Func<HttpMessageHandler, T> factory) where T : HttpClient
        {
            var client = factory(_handlerMock);
            client.BaseAddress = DefaultBaseAddress;
            return client;
        }

        public void SetupResponse<T>(Func<T> responseFactory) =>
            SetupResponse(Arg.Any<HttpRequestMessage>(), responseFactory);

        public void SetupResponse<T>(HttpMethod method, string path, Func<T> responseFactory) =>
            SetupResponse(
                Arg.Is<HttpRequestMessage>(m => m.Method == method && m.RequestUri!.AbsolutePath.Contains(path)),
                responseFactory);

        public void SetupResponse<T>(HttpRequestMessage requestMesssage, Func<T> responseFactory)
        {
            _handlerMock.GetType().GetMethod("SendAsync", BindingFlags.NonPublic | BindingFlags.Instance)!
                .Invoke(_handlerMock, new object?[] {requestMesssage, Arg.Any<CancellationToken>()})
                .Returns(Task.FromResult(GetResponse(responseFactory)));
        }

        private static HttpResponseMessage GetResponse<T>(Func<T> responseFactory)
        {
            var result = responseFactory();
            return new()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(result is string stringValue
                    ? stringValue
                    : JsonSerializer.Serialize(result))
            };
        }
    }

    public class HttpMockTest
    {
        [Fact]
        public async Task Vervangt_http_get_calls()
        {
            const string response = "Dit is geweldig!";
            var client = MockedClient(response);

            var result = await client.GetAsync("doemaarwat");

            Assert.Equal(response, await result.Content.ReadAsStringAsync());
        }

        [Fact]
        public async Task Vervangt_http_post_calls()
        {
            const string response = "Dit is geweldig!";
            var client = MockedClient(response);

            var result = await client.PostAsync("doemaarwat", new StringContent("Wat anders"));

            Assert.Equal(response, await result.Content.ReadAsStringAsync());
        }

        [Fact]
        public async Task Vervangt_http_put_calls()
        {
            const string response = "Dit is geweldig!";
            var client = MockedClient(response);

            var result = await client.PutAsync("doemaarwat", new StringContent("Wat anders"));

            Assert.Equal(response, await result.Content.ReadAsStringAsync());
        }

        [Fact]
        public async Task Vervangt_http_delete_calls()
        {
            const string response = "Dit is geweldig!";
            var client = MockedClient(response);

            var result = await client.DeleteAsync("doemaarwat");

            Assert.Equal(response, await result.Content.ReadAsStringAsync());
        }

        [Fact]
        public async Task Vervangt_specifieke_requests()
        {
            const string response = "Dit is geweldig!";
            const string path = "doemaarwat";
            var httpMock = new HttpMock();
            httpMock.SetupResponse(HttpMethod.Post, path, () => response);

            var result = await httpMock.Client().PostAsync(path, new StringContent("Wat anders"));

            Assert.Equal(response, await result.Content.ReadAsStringAsync());
        }

        private static HttpClient MockedClient(string response)
        {
            var httpMock = new HttpMock();
            httpMock.SetupResponse(() => response);
            var client = httpMock.Client();
            return client;
        }
    }
}
