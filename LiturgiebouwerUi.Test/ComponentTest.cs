using Bunit;
using Microsoft.AspNetCore.Components;

namespace LiturgiebouwerUi.Test
{
    public abstract class ComponentTest<T> : TestContext where T : IComponent
    {
        protected IRenderedComponent<T> Render(params ComponentParameter[] parameters) => RenderComponent<T>(parameters);
    }
}