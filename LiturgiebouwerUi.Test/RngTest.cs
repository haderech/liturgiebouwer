﻿using LiturgiebouwerUi.Models;

namespace LiturgiebouwerUi.Test
{
    public abstract class RngTest
    {
        protected RngTest()
        {
            Rng.Seed = () => 123;
        }
    }
}
