using System.Linq;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using LiturgiebouwerUi.Models;
using LiturgiebouwerUi.Services.Implementations;
using LiturgiebouwerUi.Test.TestHelper;
using Xunit;
using NSubstitute;

namespace LiturgiebouwerUi.Test.Services
{
    public class BibleServiceTest
    {
        private readonly BibleService _sut;
        private readonly ILocalStorageService _localStorageServiceMock;
        private readonly HttpMock _httpMock;

        public BibleServiceTest()
        {
            _localStorageServiceMock = Substitute.For<ILocalStorageService>();
            _httpMock = new HttpMock();
            _sut = new BibleService(_localStorageServiceMock, _httpMock.Client());
        }

        [Fact]
        public async Task Haalt_alle_bijbelboeken_op()
        {
            var book = Defaults.Book;
            _httpMock.SetupResponse(() => new[] { book });

            var books = await _sut.GetBooks();

            Assert.Equal(book.Name, books.Single().Name);
        }

        [Fact]
        public async Task Stuurt_een_event_bij_nieuwe_bijbelkeuze()
        {
            const Bible newBible = Bible.Statenvertaling;
            Bible? result = null;
            _sut.RaiseBibleChanged += (_, e) => result = e.NewBible;

            await _sut.SetPreference(newBible);

            Assert.Equal(newBible, result);
        }

        [Fact]
        public async Task Slaat_nieuwe_bijbelkeuze_op()
        {
            await _sut.SetPreference(Bible.Statenvertaling);

            await _localStorageServiceMock.Received().SetItemAsync(Arg.Any<string>(), Bible.Statenvertaling, default);
        }

        [Fact]
        public async Task Haalt_keuze_uit_opslag()
        {
            const Bible bible = Bible.Statenvertaling;
            _localStorageServiceMock.GetItemAsync<Bible?>(Arg.Any<string>(), default)
                .Returns(bible);

            var result = await _sut.GetPreference();

            Assert.Equal(bible, result);
        }
    }
}