using System.Linq;
using System.Threading.Tasks;
using LiturgiebouwerUi.Models;
using LiturgiebouwerUi.Parashot.Models;
using LiturgiebouwerUi.Parashot.Services.Implementations;
using LiturgiebouwerUi.Services;
using LiturgiebouwerUi.Test.TestHelper;
using NSubstitute;
using Xunit;

namespace LiturgiebouwerUi.Test.Services
{
    public class ParashaServiceTest
    {
        private readonly IBibleService _bibleServiceMock;
        private readonly HttpMock _httpMock;
        private readonly ParashaService _sut;

        public ParashaServiceTest()
        {
            _httpMock = new HttpMock();
            _bibleServiceMock = Substitute.For<IBibleService>();
            _sut = new ParashaService(_bibleServiceMock, _httpMock.Client());
        }

        [Fact]
        public async Task Bepaalt_alle_hoofdstukken_wanneer_er_geen_hoofstukken_zijn_gespecificeerd()
        {
            var book = new Book(3, "Boekje");
            MockBibleBooks(book);
            MockTorahPortions(new ParashaDto("Deze", new[] { "Boekje" }));

            var result = await _sut.GetTorahPortions();

            Assert.Collection(result.Single().Portions,
                p => AssertParasha(p, book, 1),
                p => AssertParasha(p, book, 2),
                p => AssertParasha(p, book, 3));
        }

        [Fact]
        public async Task Bepaalt_versen_voor_een_enkel_hoofstuk_wanneer_er_een_enkele_is()
        {
            var book = new Book(3, "Boekje");
            MockBibleBooks(book);
            MockTorahPortions(new ParashaDto("Deze", new[] { "Boekje 1:1-5" }));

            var result = await _sut.GetTorahPortions();

            Assert.Collection(result.Single().Portions,
                p => AssertParasha(p, book, 1, 1, 5));
        }

        [Fact]
        public async Task Bepaalt_versen_voor_twee_hoofstukken_wanneer_er_twee_zijn()
        {
            var book = new Book(3, "Boekje");
            MockBibleBooks(book);
            MockTorahPortions(new ParashaDto("Deze", new[] { "Boekje 1:1-2:5" }));

            var result = await _sut.GetTorahPortions();

            Assert.Collection(result.Single().Portions,
                p => AssertParasha(p, book, 1, expectedFrom: 1),
                p => AssertParasha(p, book, 2, expectedTo: 5));
        }

        [Fact]
        public async Task Combineert_meerdere_bijbelboeken()
        {
            var book = new Book(3, "Boekje");
            var otherBook = new Book(2, "Deze ook");
            MockBibleBooks(book, otherBook);
            MockTorahPortions(new ParashaDto("Andere", new[] { "Boekje 1:1-5", "Deze ook 1:1-2:5" }));

            var result = await _sut.GetTorahPortions();

            Assert.Collection(result.First().Portions,
                p => AssertParasha(p, book, 1, 1, 5),
                p => AssertParasha(p, otherBook, 1),
                p => AssertParasha(p, otherBook, 2, 1, 5));
        }

        [Fact]
        public async Task Selecteerd_verschillende_hoofdstukken_uit_1_boek()
        {
            var book = new Book(3, "Boekje");
            MockBibleBooks(book);
            MockTorahPortions(new ParashaDto("Andere", new[] { "Boekje 1:1-5,3:4-9" }));

            var result = await _sut.GetTorahPortions();

            Assert.Collection(result.First().Portions,
                p => AssertParasha(p, book, 1, 1, 5),
                p => AssertParasha(p, book, 3, 4, 9));
        }

        private void MockTorahPortions(params ParashaDto[] dtos) => _httpMock.SetupResponse(() => dtos);

        private void MockBibleBooks(params Book[] books) =>
            _bibleServiceMock.GetBooks()
                .Returns(books);

        private static void AssertParasha((Chapter chapter, short? from, short? to) actual, Book expectedBook,
            short expectedChapter, short expectedFrom = 1, short? expectedTo = null)
        {
            Assert.Equal((new Chapter(expectedBook, expectedChapter), expectedFrom, expectedTo), actual);
        }
    }
}
