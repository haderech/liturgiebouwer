using System.Threading.Tasks;
using LiturgiebouwerUi.Services.Implementations;
using LiturgiebouwerUi.Test.Extensions;
using Microsoft.JSInterop;
using NSubstitute;
using Xunit;

namespace LiturgiebouwerUi.Test.Services
{
    public class GenerateDownloadServiceTest
    {
        private readonly GenerateDownloadService _sut;
        private readonly IJSRuntime _jsRuntimeMock;

        public GenerateDownloadServiceTest()
        {
            _jsRuntimeMock = Substitute.For<IJSRuntime>();
            _sut = new GenerateDownloadService(_jsRuntimeMock);
        }

        [Fact]
        public async Task Roept_de_saveFile_javascript_methode_aan()
        {
            const string type = "text";
            const string name = "Henkies";
            const string content = "Jij bent echt een Henkie";

            await _sut.GenerateDownload(type, name, content);

            await _jsRuntimeMock.Received()
                .InvokeVoidAsync("saveFile",Arg.Is<object[]>(a => a.SequenceEqual(type, name, content)));
        }
    }
}

