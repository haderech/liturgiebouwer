using LiturgiebouwerUi.Models;
using LiturgiebouwerUi.Parashot.Services.Implementations;
using Microsoft.AspNetCore.Components;
using Xunit;

namespace LiturgiebouwerUi.Test.Services
{
    public class ParashaCalendarServiceTest
    {
        private readonly ParashaCalendarService _sut;

        public ParashaCalendarServiceTest()
        {
            _sut = new ParashaCalendarService(NavigationManagerMock.Instance);
        }

        [Fact]
        public void Maakt_een_kalender()
        {
            var kalender =
                _sut.Create(new[] { new Parasha("Henk", new[] { (Defaults.Chapter, (short)1, (short?)10) }) });

            Assert.NotNull(kalender);
        }

        private class NavigationManagerMock : NavigationManager
        {
            public static NavigationManagerMock Instance => new();

            private NavigationManagerMock()
            {
                Initialize("https://localhost/", "https://localhost/");
            }

            protected override void NavigateToCore(string uri, bool forceLoad)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
