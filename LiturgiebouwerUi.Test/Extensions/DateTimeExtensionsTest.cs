using System;
using System.Globalization;
using System.Threading;
using LiturgiebouwerUi.Extensions;
using Xunit;

namespace LiturgiebouwerUi.Test.Extensions
{
    public class DateTimeExtensionsTest
    {
        [Fact]
        public void Returns_a_date_in_Hebrew()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("nl");

            var date = new DateTime(2021, 9, 27);

            var result = date.ToHebrewDateString();

            Assert.Equal("5782 תשרי 21", result);
        }
    }
}
