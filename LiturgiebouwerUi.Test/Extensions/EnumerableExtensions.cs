using System.Collections.Generic;
using System.Linq;
using LiturgiebouwerUi.Test.Extensions;

namespace LiturgiebouwerUi.Test.Extensions;

public static class EnumerableExtensions
{
    public static bool SequenceEqual<T>(this IEnumerable<T> enumerator, params T[] values)
    {
        return enumerator.SequenceEqual(values.AsEnumerable());
    }
}

