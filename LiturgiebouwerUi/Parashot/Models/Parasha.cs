using System.Collections.Generic;

namespace LiturgiebouwerUi.Models
{
    public record Parasha(string Name,
        IReadOnlyCollection<(Chapter chapter, short fromVerse, short? toVerse)> Portions);
}