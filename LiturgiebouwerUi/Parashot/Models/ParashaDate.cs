using System;
using System.Globalization;
using LiturgiebouwerUi.Extensions;

namespace LiturgiebouwerUi.Parashot.Models
{
    public static class ParashaDate
    {
        public static readonly HebrewCalendar Calendar = new();

        public static int CurrentYear => Calendar.GetYear(DateTime.Today);

        public static DateTime DateForWeek(int weekNumber) =>
            DateTime.Today.GetLocalBeginDateTimeForParashaWeek(weekNumber);
    }
}
