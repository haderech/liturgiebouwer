using System.Collections.Generic;

namespace LiturgiebouwerUi.Parashot.Models
{
    public record ParashaDto(string Name, IEnumerable<string> Portions);
}