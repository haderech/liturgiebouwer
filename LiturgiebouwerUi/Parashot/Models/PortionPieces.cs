using System;
using System.Collections.Generic;
using System.Linq;

namespace LiturgiebouwerUi.Parashot.Models
{
    public class PortionPieces
    {
        private const short FirstVerse = 1;
        public string BookAbbreviation { get; }
        public IReadOnlyCollection<(short chapter, short fromVerse, short? toVerse)> Pieces { get; }

        public PortionPieces(string portion)
        {
            var (bookAbbreviation, portions) = ParsePortion(portion);
            BookAbbreviation = bookAbbreviation;
            Pieces = portions;
        }

        private static (string bookAbbreviation, (short chapter, short fromVerse, short? toVerse)[] portions)
            ParsePortion(
                string portion)
        {
            var (bookAbbreviation, verses) = GetBookAndVerses(portion);
            if (verses.Equals(string.Empty))
            {
                return WholeBook(bookAbbreviation);
            }

            if (PortionIsWholeChapter(verses))
            {
                return WholeChapter(bookAbbreviation, verses);
            }

            var parsedVerses = verses.Split(',')
                .Select(v => v.Split('-'))
                .SelectMany(ParseVerses)
                .ToArray();
            return (bookAbbreviation, parsedVerses);
        }

        private static IEnumerable<(short, short, short?)> ParseVerses(string[] versesParts)
        {
            var fromPart = versesParts.First().Split(':').Select(short.Parse).ToArray();
            var toPart = versesParts.Last().Split(':').Select(short.Parse).ToArray();
            return PortionIsFromOneChapter(toPart)
                ? VersesInChapter(fromPart, toPart.Single())
                : VersesFromChapters(fromPart, toPart);
        }

        private static (string bookAbbreviation, string verses) GetBookAndVerses(string portion)
        {
            var lastSpaceIndex = portion.LastIndexOf(' ');
            var wholeBook = (portion, string.Empty);
            if (lastSpaceIndex == -1)
            {
                return wholeBook;
            }

            var versesPart = portion[(lastSpaceIndex + 1)..];
            return PortionPartContainsNoVerseInformation(versesPart)
                ? wholeBook
                : (portion[..lastSpaceIndex].Trim('.'), versesPart);
        }

        private static (string bookAbbreviation, (short chapter, short fromVerse, short? toVerse)[]) WholeBook(
            string portion) => (
            portion.Trim('.'), Array.Empty<(short chapter, short fromVerse, short? toVerse)>());

        private static (string bookAbbreviation, (short chapter, short FirstVerse, short? toVerse)[]) WholeChapter(
            string bookAbbreviation, string verses) => (bookAbbreviation,
            new[] { (short.Parse(verses), FirstVerse, null as short?) });

        private static (short, short, short?)[] VersesInChapter(IReadOnlyList<short> fromPieces, short toVerse) =>
            new[] { (fromPieces[0], fromPieces[1], toVerse as short?) };

        private static (short chapter, short fromVerse, short? toVerse)[] VersesFromChapters(
            IReadOnlyList<short> fromPieces, IReadOnlyList<short> toPieces) =>
            Enumerable.Range(fromPieces[0], toPieces[0] - fromPieces[0] + 1)
                .Select(c => (
                    (short)c,
                    c == fromPieces[0] ? fromPieces[1] : FirstVerse,
                    c == toPieces[0] ? toPieces[1] as short? : null))
                .ToArray();

        private static bool PortionIsFromOneChapter(IList<short> versesPieces) => versesPieces.Count == 1;

        private static bool PortionIsWholeChapter(string verses) => !verses.Contains(':') && !verses.Contains('-');

        private static bool PortionPartContainsNoVerseInformation(string portionPart) =>
            portionPart.All(c => !short.TryParse(c.ToString(), out _));
    }
}