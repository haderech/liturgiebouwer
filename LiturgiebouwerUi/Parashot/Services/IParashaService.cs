using System.Collections.Generic;
using System.Threading.Tasks;
using LiturgiebouwerUi.Models;

namespace LiturgiebouwerUi.Parashot.Services
{
    public interface IParashaService
    {
        Task<IList<Parasha>> GetTorahPortions();
    }
}