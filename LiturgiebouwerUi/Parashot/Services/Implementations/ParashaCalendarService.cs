using System;
using System.Collections.Generic;
using System.Linq;
using Ical.Net.CalendarComponents;
using Ical.Net.DataTypes;
using LiturgiebouwerUi.Builders;
using LiturgiebouwerUi.Extensions;
using LiturgiebouwerUi.Models;
using LiturgiebouwerUi.Parashot.Models;
using Microsoft.AspNetCore.Components;

namespace LiturgiebouwerUi.Parashot.Services.Implementations
{
    public class ParashaCalendarService : IParashaCalendarService
    {
        private readonly NavigationManager _navigationManager;

        public ParashaCalendarService(NavigationManager navigationManager)
        {
            _navigationManager = navigationManager;
        }

        public string Create(IList<Parasha> parashot) =>
            new CalendarBuilder()
                .AddEvents(parashot.Select(CreateCalendarEvent).ToArray())
                .Serialize();

        private CalendarEvent CreateCalendarEvent(Parasha parasha, int index)
        {
            var parashaBeginDate = DateTime.Today.GetLocalBeginDateTimeForParashaWeek(index + 1);
            return new CalendarEvent
            {
                Start = new CalDateTime(parashaBeginDate),
                Duration = TimeSpan.FromDays(7),
                Summary = parasha.Name,
                Description = GetDescription(parasha),
                Url = new Uri(_navigationManager.Uri),
                Uid = $"{ParashaDate.CurrentYear}{parasha.Name}",
                LastModified = new CalDateTime(DateTime.Now)
            };
        }

        private static string GetDescription(Parasha parasha) =>
            string.Join(Environment.NewLine, parasha.Portions.GroupBy(p => p.chapter.Book)
                .Select(b =>
                    $"{b.Key.Name} {string.Join(", ", b.Select(p => GetVersesDescription(p.chapter.ChapterNumber, p.fromVerse, p.toVerse)))}"));

        private static string GetVersesDescription(short chapter, short fromVerse, short? toVerse)
        {
            var verses = toVerse.HasValue ? $":{fromVerse}-{toVerse}" : "";
            return $"{chapter}{verses}";
        }
    }
}
