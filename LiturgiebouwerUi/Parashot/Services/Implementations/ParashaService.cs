using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LiturgiebouwerUi.Models;
using LiturgiebouwerUi.Parashot.Models;
using LiturgiebouwerUi.Services;

namespace LiturgiebouwerUi.Parashot.Services.Implementations
{
    public class ParashaService : IParashaService
    {
        private readonly IBibleService _bibleService;
        private readonly HttpClient _http;

        public ParashaService(IBibleService bibleService, HttpClient http)
        {
            _http = http;
            _bibleService = bibleService;
        }

        public async Task<IList<Parasha>> GetTorahPortions()
        {
            var dtos = await _http.GetFromJsonAsync<IList<ParashaDto>>("parasha.json")
                       ?? Array.Empty<ParashaDto>();
            var books = await _bibleService.GetBooks();
            return dtos.Select(t => Parse(t, books)).ToList();
        }

        private static Parasha Parse(ParashaDto dto, IList<Book> books) =>
            new(dto.Name, dto.Portions.SelectMany(p => GetPortions(p, books)).ToArray());

        private static IList<(Chapter chapter, short from, short? to)> GetPortions(string portion, IList<Book> books)
        {
            var pieces = new PortionPieces(portion);
            var book = books.SingleOrDefault(b => b.Name.StartsWith(pieces.BookAbbreviation));
            if (book is default(Book))
            {
                throw new IndexOutOfRangeException($"Er is geen boek die begint met {pieces.BookAbbreviation}");
            }

            return pieces.Pieces.Any()
                ? pieces.Pieces.Select(p => (new Chapter(book, p.chapter), p.fromVerse, p.toVerse)).ToList()
                : Enumerable.Range(1, book.Chapters)
                    .Select(c => (new Chapter(book, (short) c), (short) 1, null as short?))
                    .ToArray();
        }
    }
}