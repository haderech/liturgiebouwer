using System.Collections.Generic;
using LiturgiebouwerUi.Models;

namespace LiturgiebouwerUi.Parashot.Services
{
    public interface IParashaCalendarService
    {
        string Create(IList<Parasha> parashot);
    }
}
