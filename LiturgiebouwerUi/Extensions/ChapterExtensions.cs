using LiturgiebouwerUi.Models;

namespace LiturgiebouwerUi.Extensions
{
    public static class ChapterExtensions
    {
        public static string GetHerzieneStatenvertalingLink(this Chapter chapter, short? startingVerse = null)
        {
            var verseUri = startingVerse.HasValue ? $"#{startingVerse.Value}" : string.Empty;
            return
                $"https://herzienestatenvertaling.nl/teksten/{GetBookPath(chapter)}/{chapter.ChapterNumber}{verseUri}";
        }

        private static string GetBookPath(Chapter chapter) => chapter.Book.Name.Replace(" ", "").ToLower();
    }
}
