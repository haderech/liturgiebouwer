using System;
using System.Globalization;
using LiturgiebouwerUi.Models;
using LiturgiebouwerUi.Parashot.Models;

namespace LiturgiebouwerUi.Extensions
{
    public static class DateTimeExtensions
    {
        private static readonly CultureInfo HebrewCultureInfo = CreateHebrewCulture();
        private const short WeeksFromRoshHashanaToSimchaTorah = 2;

        private static CultureInfo CreateHebrewCulture()
        {
            var hebrewCulture = CultureInfo.CreateSpecificCulture("he-IL");
            hebrewCulture.DateTimeFormat.Calendar = ParashaDate.Calendar;
            return hebrewCulture;
        }

        public static string ToHebrewDateString(this DateTime value) =>
            $"{ParashaDate.Calendar.GetYear(value)} {HebrewCultureInfo.DateTimeFormat.GetMonthName(ParashaDate.Calendar.GetMonth(value))} {ParashaDate.Calendar.GetDayOfMonth(value)}";

        public static string ToLocalDateString(this DateTime value) =>
           value.ToString("d MMM yyyy");

        /// <summary>
        /// Amount of weeks passed since Simcha Torah
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int GetParashaWeek(this DateTime value) =>
            ParashaDate.Calendar.GetWeekOfYear(value, CalendarWeekRule.FirstFullWeek, DayOfWeek.Sunday) -
            WeeksFromRoshHashanaToSimchaTorah;

        public static DateTime GetLocalBeginDateTimeForParashaWeek(this DateTime value, int weekNumber)
        {
            var currentYear = ParashaDate.Calendar.GetYear(value);
            var firstDayOfYear = new DateTime(currentYear, 1, 1, ParashaDate.Calendar);
            var firstDayOfFirstWeekOfYear = firstDayOfYear.AddDays(-(short)firstDayOfYear.DayOfWeek);
            return firstDayOfFirstWeekOfYear.AddDays((weekNumber + WeeksFromRoshHashanaToSimchaTorah) * 7);
        }

        public static string GetLocalWeekDescriptionForHebrewWeek(this DateTime value, int weekNumber)
        {
            var parashaWeekFirstDay = value.GetLocalBeginDateTimeForParashaWeek(weekNumber);
            return parashaWeekFirstDay.ToShortDateString() + " - " + parashaWeekFirstDay.AddDays(6).ToShortDateString();
        }
    }
}
