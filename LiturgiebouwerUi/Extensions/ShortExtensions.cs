using System;

namespace LiturgiebouwerUi.Extensions
{
    public static class ShortExtensions
    {
        public static string GetValueOrDefault(this short? s, string defaultValue) =>
            (s.HasValue ? s.ToString() : defaultValue) ?? throw new InvalidOperationException();
    }
}
