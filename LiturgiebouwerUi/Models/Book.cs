namespace LiturgiebouwerUi.Models
{
    public record Book(short Chapters, string Name);
}
