namespace LiturgiebouwerUi.Models
{
    public record Chapter(Book Book, short ChapterNumber);
}