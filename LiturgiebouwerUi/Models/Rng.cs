using System;

namespace LiturgiebouwerUi.Models
{
    public class Rng
    {
        public static Func<int> Seed { private get; set; } = () => new Random().Next();
        private readonly Random _random;

        public Rng() => _random = new Random(Seed());

        public int Next(int maxValue) => _random.Next(maxValue);

        public short NextIncl(short maxValue) => (short)(_random.Next(maxValue) + 1);
    }
}