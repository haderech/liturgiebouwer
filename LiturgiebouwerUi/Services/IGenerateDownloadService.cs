using System.Threading.Tasks;

namespace LiturgiebouwerUi.Services
{
    public interface IGenerateDownloadService
    {
        ValueTask GenerateDownload(string type, string name, string content);
    }
}
