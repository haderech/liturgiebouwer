using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LiturgiebouwerUi.Events;
using LiturgiebouwerUi.Models;

namespace LiturgiebouwerUi.Services
{
    public interface IBibleService
    {
        event EventHandler<BibleChangedEvent> RaiseBibleChanged;
        Task<IList<Book>> GetBooks();
        ValueTask<Bible> GetPreference();
        Task SetPreference(Bible bible);
    }
}