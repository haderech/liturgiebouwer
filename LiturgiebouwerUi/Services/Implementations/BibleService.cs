using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using LiturgiebouwerUi.Events;
using LiturgiebouwerUi.Models;

namespace LiturgiebouwerUi.Services.Implementations
{
    public class BibleService : IBibleService
    {
        private const string BibleKey = "bible";
        private const string BooksKey = "books";
        private readonly ILocalStorageService _localStorage;
        private readonly HttpClient _http;

        public BibleService(ILocalStorageService localStorage, HttpClient http)
        {
            _localStorage = localStorage;
            _http = http;
        }

        public async Task<IList<Book>> GetBooks() => await _localStorage.GetItemAsync<IList<Book>>(BooksKey)
            ?? await DownloadAndSaveBooks();

        private async Task<IList<Book>> DownloadAndSaveBooks()
        {
            var books = await _http.GetFromJsonAsync<IList<Book>>("books.json") ?? new List<Book>();
            await _localStorage.SetItemAsync(BooksKey, books);
            return books;
        }

        public async ValueTask<Bible> GetPreference() =>
            await _localStorage.GetItemAsync<Bible?>(BibleKey) ?? Bible.HerzieneStatenvertaling;

        public async Task SetPreference(Bible bible)
        {
            await _localStorage.SetItemAsync(BibleKey, bible);
            RaiseBibleChanged?.Invoke(this, new BibleChangedEvent { NewBible = bible });
        }

        public event EventHandler<BibleChangedEvent>? RaiseBibleChanged;
    }
}
