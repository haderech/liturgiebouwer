using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace LiturgiebouwerUi.Services.Implementations
{
    public class GenerateDownloadService : IGenerateDownloadService
    {
        private readonly IJSRuntime _jsRuntime;

        public GenerateDownloadService(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public ValueTask GenerateDownload(string type, string name, string content) =>
            _jsRuntime.InvokeVoidAsync("saveFile", type, name, content);
    }
}
