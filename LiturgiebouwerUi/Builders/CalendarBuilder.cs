using System.Collections.Generic;
using Ical.Net;
using Ical.Net.CalendarComponents;
using Ical.Net.Serialization;

namespace LiturgiebouwerUi.Builders
{
    public class CalendarBuilder
    {
        private static readonly ComponentSerializer Serializer = new();
        private readonly List<CalendarEvent> _events = new();

        public CalendarBuilder AddEvents(IList<CalendarEvent> events)
        {
            _events.AddRange(events);
            return this;
        }

        public string Serialize() => Serializer.SerializeToString(Build());

        private Calendar Build()
        {
            var calendar = new Calendar
            {
                ProductId = "-//haderech.nl//NONSGML Parashot//NL",
                Version = "2.0"
            };
            calendar.Events.AddRange(_events);
            return calendar;
        }
    }
}
