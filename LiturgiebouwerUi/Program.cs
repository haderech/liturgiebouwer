using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using LiturgiebouwerUi;
using Blazored.LocalStorage;
using LiturgiebouwerUi.Parashot.Services;
using LiturgiebouwerUi.Parashot.Services.Implementations;
using LiturgiebouwerUi.Services;
using LiturgiebouwerUi.Services.Implementations;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(_ => new HttpClient
                    { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) })
                .AddBlazoredLocalStorage()
                .AddScoped<IBibleService, BibleService>()
                .AddScoped<IParashaService, ParashaService>()
                .AddScoped<IParashaCalendarService, ParashaCalendarService>()
                .AddScoped<IGenerateDownloadService, GenerateDownloadService>();

await builder.Build().RunAsync();
