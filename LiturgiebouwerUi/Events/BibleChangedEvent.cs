using System;
using LiturgiebouwerUi.Models;

namespace LiturgiebouwerUi.Events
{
    public class BibleChangedEvent : EventArgs
    {
        public Bible NewBible { get; init; }
    }
}